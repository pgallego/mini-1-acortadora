import webapp
import random
import shelve

PORT = 1237

form = '<form action="" method="post"><label for="url">URL a acortar:</label>' \
       + '<input id="url" type="text" name="url">' \
       + '<input type="submit" value="Enviar"></form>'

short_urls = shelve.open('short_urls')


class ErrorPages:
    page = "<html><body>" + "<h1> Error <br> </h1>"

    def __init__(self, details):
        self.page += f"<h2> {details} </h2>" + "</body> </html>"

    def __str__(self):
        return self.page


class Shortener(webapp.WebApp):
    # short_urls = {}  # {short_url: original_url}
    short_urls = short_urls

    def parse(self, received):
        recibido = received.decode()
        metodo = recibido.split(' ')[0]
        recurso = recibido.split(' ')[1]
        if metodo == "POST":
            body = recibido.split('\r\n\r\n')[1]
        else:
            body = None
        return metodo, recurso, body

    def format_dict(self):
        formatted = ""
        for key in self.short_urls.keys():
            formatted += f'<ul><li>{self.short_urls[key]}  :  <a href="{key}">{key}</a></li></ul>'
        return formatted

    def shorten(self, url, html):
        new = 0  # If url has already been shortened
        if url in self.short_urls.values():
            html += f"<br>La URL {url} ya ha sido acortada"
            already_sh = str([k for k, v in self.short_urls.items() if v == url])
            html += f' a <a href = "{already_sh[2:len(already_sh)-2]}"> {already_sh[2:len(already_sh)-2]}</a><br><br>'
        else:
            shortened = "http://localhost:" + str(PORT) + "/" + str(random.randint(1, 100000))
            self.short_urls[shortened] = url
            self.short_urls.sync()
            new = 1  # If url is new
        return html, new

    def process(self, analyzed):
        metodo, recurso, body = analyzed
        http = '200 OK'
        html = '<html><body>' + form + '<h4>Lista de URLs acortadas:</h4>' + self.format_dict()

        if recurso != "/":
            if ("http://localhost:" + str(PORT) + recurso) in self.short_urls.keys():
                http = '302 Moved Temporarily'
                html = '<html><head><meta http-equiv="refresh" content="0;url=' \
                       + str(self.short_urls[("http://localhost:" + str(PORT) + recurso)]) + '"</head></body></hmtl>' \
                       + "</body></html>"
            else:
                http = "404 Not Found"
                html = str(ErrorPages("Recurso no disponible"))

        if metodo == "POST":
            if body.split('=')[1]:
                url = body.split('=')[1].replace("%2F", "/").replace("%3A", ":")
                if not (url.startswith("https://") or url.startswith("http://")):
                    url = "https://" + url
                html, new = self.shorten(url, html)
                sh = str([k for k, v in self.short_urls.items() if v == url])
                if new:
                    html = html + f'<ul><li>{url}  :  <a href="{sh[2:len(sh)-2]}">{sh[2:len(sh)-2]}</a' \
                                  f'></li></ul>' + "</body></html>"
            else:
                http = "404 Not Found"
                html = str(ErrorPages("Formulario vac&iacute;o"))
        return http, html


if __name__ == "__main__":
    testshortener = Shortener('localhost', PORT)
